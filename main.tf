terraform {
  required_version =">=1.0.0"  
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.98.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "myrgss" {
  name     = "myrgss-1"
  location = "East US"
}

resource "azurerm_virtual_network" "myvnetss" {
  name                = "vnetss-1"
  resource_group_name = azurerm_resource_group.myrgss.name
  location            = azurerm_resource_group.myrgss.location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "mysubnetss" {
  name                 = "subnetss-1"
  resource_group_name  = azurerm_resource_group.myrgss.name
  virtual_network_name = azurerm_virtual_network.myvnetss.name
  address_prefixes     = ["10.0.2.0/24"]
}

// The public IP address is going to be assigned to the load balancer
resource "azurerm_public_ip" "mylbip" {
  name                = "myload-ip"
  location            = azurerm_resource_group.myrgss.location
  resource_group_name = azurerm_resource_group.myrgss.name
  allocation_method   = "Static"
  sku="Standard"
}
# Load balancer
resource "azurerm_lb" "myloadbalancer" {
  name                = "mylb-1"
  location            = azurerm_resource_group.myrgss.location
  resource_group_name = azurerm_resource_group.myrgss.name
  sku="Standard"
  sku_tier = "Regional"
  frontend_ip_configuration {
    name                 = "frontend-ip"
    public_ip_address_id = azurerm_public_ip.mylbip.id
  }
}
// Here we are defining the backend pool
resource "azurerm_lb_backend_address_pool" "mybpool" {
  loadbalancer_id = azurerm_lb.myloadbalancer.id
  name            = "mybpool-1"
}
// Here we are defining the Health Probe
resource "azurerm_lb_probe" "myProbe" {
  resource_group_name = azurerm_resource_group.myrgss.name
  loadbalancer_id     = azurerm_lb.myloadbalancer.id
  name                = "myprobe-1"
  port                =  80
  protocol            =  "Tcp"
}
// Here we are defining the Load Balancing Rule
resource "azurerm_lb_rule" "myrule" {
  resource_group_name            = azurerm_resource_group.myrgss.name
  loadbalancer_id                = azurerm_lb.myloadbalancer.id
  name                           = "myrule-1"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "frontend-ip"
  backend_address_pool_ids = [ azurerm_lb_backend_address_pool.mybpool.id ]
}

resource "azurerm_windows_virtual_machine_scale_set" "myvmss" {
  name                = "myvmss-1"
  resource_group_name = azurerm_resource_group.myrgss.name
  location            = azurerm_resource_group.myrgss.location
  sku                 = "Standard_D2s_v3"
  instances           = 
  admin_password      = "ramakala@123"
  admin_username      = "ramakala"
  upgrade_mode = "Automatic"
  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }


  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  network_interface {
    name    = "mynicss"
    primary = true

    ip_configuration {
      name      = "internal"
      primary   = true
      subnet_id = azurerm_subnet.mysubnetss.id
      load_balancer_backend_address_pool_ids =[azurerm_lb_backend_address_pool.mybpool.id]
    }
  }
}
resource "azurerm_network_security_group" "mynsg" {
  name                = "mynsg-1"
  location            = azurerm_resource_group.myrgss.location
  resource_group_name = azurerm_resource_group.myrgss.name

#  Rule to allow traffic on port 80
  security_rule {
    name                       = "Allow_HTTP"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "mynsgass" {
  subnet_id                 = azurerm_subnet.mysubnetss.id
  network_security_group_id = azurerm_network_security_group.mynsg.id
  
}


